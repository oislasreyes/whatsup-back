import mongoose from "mongoose";

const userSchema = mongoose.Schema({
  firstName: String,
  lastName: String,
  email: {
    type: String,
    required: true,
    unique: true,
    validate: async (value) => {
      try{
        const result = await userModel.findOne({email: value})
        if (result) throw new Error("Ya existe un usuario con el correo: " + value);
      }catch(error){
        throw new Error(error);
      }
    }
  },
  username: String,
  photoUrl: String,
  uid: String
});

//collection
const userModel = mongoose.model("users", userSchema);
export default userModel;
