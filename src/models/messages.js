import mongoose from "mongoose";

const whatsappSchema = mongoose.Schema({
  userId: mongoose.ObjectId,
  conversationId: mongoose.ObjectId,
  message: String,
  name: String,
  timestamp: Date,
  received: Boolean,
});

//collection
export default mongoose.model("messages", whatsappSchema);
