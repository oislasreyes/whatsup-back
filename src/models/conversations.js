import mongoose from "mongoose";

const conversationSchema = mongoose.Schema({
  members: [{ type: mongoose.Schema.Types.ObjectId }]
});

//collection
export default mongoose.model("conversations", conversationSchema);
