import express from "express";
import {getConversations, getConversation, addConversation, deleteConversation} from "../controllers/conversations.js";
const router = express.Router();

router.get("/users/:id/conversations", getConversations); //Obtener las conversaciones de un usuario
router.get("/conversations/:id", getConversation); //Obtener el detalle de una conversación
router.post("/conversations", addConversation); //Agregar una conversación
router.delete("/conversations/:id", deleteConversation); //Eliminar una conversación

export default router;