import express from "express";
import {getMessages, addMessage, deleteMessages} from "../controllers/messages.js";
const router = express.Router();

router.get("/conversations/:id/messages", getMessages); //Obtener todos los mensajes
router.post("/messages", addMessage); //Agregar un mensaje
router.delete("/messages/:id", deleteMessages); //Eliminar un mensaje

export default router;