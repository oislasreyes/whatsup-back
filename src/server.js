import express from "express";
import mongoose from "mongoose";
import Pusher from "pusher";
import cors from "cors";
import morgan from "morgan";
import dotenv from "dotenv";

dotenv.config();

//Routes
import userRoute from "./routes/users.js";
import conversationsRoute from "./routes/conversations.js";
import messagesRoute from "./routes/messages.js";

//app config
const app = express();
const port = process.env.PORT || 9000;

const pusher = new Pusher({
  appId: process.env.PUSHER_API_ID,
  key: process.env.PUSHER_KEY,
  secret: process.env.PUSHER_SECRET,
  cluster: "us2",
  useTLS: true
});

// middleware
app.use(morgan('combined'));
app.use(express.json());
app.use(cors());
app.get("/", (req, res) => {
  res.write("<h1>Academlo API</h1>");
  res.end();
});

// DB config
const connection_url = process.env.MONGO_CONNECTION;
mongoose.connect(connection_url, {useNewUrlParser: true, useUnifiedTopology: true });

// pusher
const db = mongoose.connection;

db.once("open", () => {
  console.log("DB connected");

  const msgCollection = db.collection("messagecontents");
  const changeStream = msgCollection.watch();

  changeStream.on("change", (change) => {
    console.log("A change occured", change);

    if (change.operationType === "insert") {
      const messageDetails = change.fullDocument;
      pusher.trigger("message", "inserted", {
        name: messageDetails.name,
        message: messageDetails.message,
        timestamp: messageDetails.timestamp,
        received: messageDetails.received,
      });
    } else {
      console.log("Error triggering Pusher");
    }
  });
});

// api routes
app.use(express.urlencoded({extended: true}));
app.use(express.json());

app.get("/api/v1/", (req, res) => {
  res.json({
    message: "API Whatsup Academlo"
  })
});

app.use("/api/v1/", userRoute);
app.use("/api/v1/", conversationsRoute);
app.use("/api/v1/", messagesRoute);

// listen
app.listen(port, () => console.log(`Escuchando sobre el puerto:${port}`));
