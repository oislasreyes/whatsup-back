import messagesModel from "../models/messages.js";
import conversationModel from "../models/conversations.js";
import mongoose from "mongoose";
import Pusher from "pusher";

const pusher = new Pusher({
  appId: process.env.PUSHER_API_ID,
  key: process.env.PUSHER_KEY,
  secret: process.env.PUSHER_SECRET,
  cluster: "us2",
  useTLS: true
});

export const getMessages = async (req, res) => {
  let { id } = req.params;
  console.log(id);
  try {
    conversationModel
      .aggregate([
        {
            $match: {
                _id: mongoose.Types.ObjectId(id),
            }
        },
        {
          $lookup: {
            from: "messages",
            localField: "_id",
            foreignField: "conversationId",
            as: "messages",
          },
        },
      ])
      .exec(function (err, conversations) {
        if (!err) {
          //Transformar respuesta
          res.json(conversations);
        }else{
          res.status(401).json({
            message: "Hubo un error al obtener las conversaciones",
          });
        }
      });
  } catch (error) {
    res.status(401).json({
      message: "Hubo un error al obtener las conversaciones",
    });
  }
};

export const addMessage = async (req, res) => {
  try {
    let { userId, conversationId, message, name, timestamp, received } = req.body;
    let response = await messagesModel.create({
        userId,
        conversationId,
        message,
        name,
        timestamp,
        received
    });

    pusher.trigger(conversationId, "inserted", {
      id: response._id,
      conversationId,
      message: message,
      timestamp: timestamp,
      received: false,
      userId,
    });

    res.json(response);
  } catch (error) {
    console.log(error);
    res.status(401).json({
      message:
        "Hubo un error al tratar de agregar a los miembros en la conversacion en el sistema",
    });
  }
};

export const deleteMessages = async (req, res) => {
  let { id } = req.params;
  try {
    let response = await messagesModel.deleteOne({ _id: id });
    pusher.trigger(conversationId, "deleted", {message: "Se ha eliminado un mensaje con el id" + response._id, deletedMessage: response._id});
    if(response.deletedCount === 1){
        res.json({messages: "El mensaje se ha eliminado correctamente"});
    }else{
        res.json({messages: "No se ha encontrado el mensaje"});
    }
  } catch (error) {
    res.status(401).json({
      message: "Hubo un error al tratar de eliminar el mensaje en el sistema",
    });
  }
};
