import userModel from "../models/users.js";

export const getUsers = async (req, res) => {
  try {
    let users = await userModel.find();
    res.json(users);
  } catch (error) {
    res.status(401).json({
      message: "Hubo un error al obtener los usuarios",
    });
  }
};

export const getUserById = async (req, res) => {
  let { id } = req.params;
  try {
    let user = await userModel.findOne({ _id: id });
    res.json(user);
  } catch (error) {
    res.status(401).json({
      message: "Hubo un error al obtene el usuario",
    });
  }
};

export const addUser = async (req, res) => {
  try {
    let { firstName, lastName, email, username, photoUrl, uid } = req.body;
    let response = await userModel.create({
      firstName,
      lastName,
      email,
      username,
      photoUrl,
      uid,
    });
    res.json(response);
  } catch (error) {
    res.status(401).json({
      message: "Hubo un error al tratar de agregar al usuario en el sistema",
      error: error.message
    });
  }
};

export const updateUser = async (req, res) => {
  let { id } = req.params;
  try {
    let { firstName, lastName, email, username, photoUrl, uid } = req.body;
    let response = await userModel.findOneAndUpdate(
      { _id: id },
      {
        firstName,
        lastName,
        email,
        username,
        photoUrl,
        uid,
      },
      { new: true }
    );
    res.json(response);
  } catch (error) {
    res.status(401).json({
      message: "Hubo un error al tratar de actualizar el usuario en el sistema",
    });
  }
};

export const deleteUser = async (req, res) => {
  let { id } = req.params;
  try {
    let response = await userModel.deleteOne({ _id: id });
    res.json(response);
  } catch (error) {
    res.status(401).json({
      message: "Hubo un error al tratar de eliminar el usuario en el sistema",
    });
  }
};
