import conversationModel from "../models/conversations.js";
import mongoose from "mongoose";

export const getConversations = async (req, res) => {
  let { id } = req.params;
  try {
    conversationModel
      .aggregate([
        {
          $lookup: {
            from: "users",
            localField: "members",
            foreignField: "_id",
            as: "membersObj",
          },
        },
      ])
      .exec(function (err, conversations) {
        if (!err) {
          //Transformar respuesta
          let conversationDetail;
          conversations.forEach((convers, index) => {
            conversationDetail = convers.membersObj.find(
              (member) => member._id != id
            );
            conversations[index]["info"] = conversationDetail
              ? conversationDetail
              : {};
          });
          res.json(conversations);
        }else{
          res.status(401).json({
            message: "Hubo un error al obtener las conversaciones",
          });
        }
      });
  } catch (error) {
    res.status(401).json({
      message: "Hubo un error al obtener las conversaciones",
    });
  }
};

export const getConversation = async (req, res) => {
  let { id } = req.params;
  console.log(id);
  try {
    conversationModel
      .aggregate([
        {
          $match: {
            _id: mongoose.Types.ObjectId(id),
          },
        },
        {
          $lookup: {
            from: "users",
            localField: "members",
            foreignField: "_id",
            as: "membersObj",
          },
        },
      ])
      .exec(function (err, conversations) {
        // Transformar respuesta
        let conversationDetail;
        conversations.forEach((convers, index) => {
          conversationDetail = convers.membersObj.find(
            (member) => member._id != id
          );
          conversations[index]["info"] = conversationDetail
            ? conversationDetail
            : {};
        });
        conversations =
          conversations.length > 0 ? conversations[0] : conversations;
        res.json(conversations);
      });
  } catch (error) {
    res.status(401).json({
      message: "Hubo un error al obtener las conversaciones",
    });
  }
};

export const addConversation = async (req, res) => {
  try {
    let { members } = req.body;
    console.log(members);
    let response = await conversationModel.create({
      members,
    });
    res.json(response);
  } catch (error) {
    console.log(error);
    res.status(401).json({
      message:
        "Hubo un error al tratar de agregar a los miembros en la conversacion en el sistema",
    });
  }
};

export const deleteConversation = async (req, res) => {
  let { id } = req.params;
  try {
    let response = await conversationModel.deleteOne({ _id: id });
    res.json(response);
  } catch (error) {
    res.status(401).json({
      message: "Hubo un error al tratar de actualizar el usuario en el sistema",
    });
  }
};
